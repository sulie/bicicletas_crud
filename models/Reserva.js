let mongoose = require('mongoose');
let moment = require('moment');
let Schema = mongoose.Schema;

let reservaSchema = new Schema({
    desde: Date,
    hasta: Date,

    bicicleta: { type: mongoose.Schema.Types.ObjectId, ref: "Bicicleta" },
    usuario: { type: mongoose.Schema.Types.ObjectId, ref: "Usuario" }

});

reservaSchema.statics.allReservas = function (cb) {
    return this.find({}, cb);
}

reservaSchema.statics.add = function (aReserva, cb) {
    return this.create(aReserva, cb);
}

reservaSchema.statics.findById = function (reservaId, cb) {
    return this.findOne({_id: reservaId}, cb);
}

reservaSchema.statics.removeById = function (reservaId, cb) {
    return this.deleteOne({_id: reservaId}, cb);
}

//Cuántos días está reservada la bicicleta
reservaSchema.methods.diasDeReserva = function () {
    return moment(this.hasta.diff(moment(this.desde), "days") + 1);

};

module.exports = mongoose.model("Reserva", reservaSchema);