let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let Bicicleta = function (bicicletaID, color, modelo, ubicacion) {
    // Constructor
    this.bicicletaID = bicicletaID;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

let bicicletaSchema = new Schema({
    bicicletaID: Number,
    color: String,
    modelo: String,
    ubicacion: {type: [Number], index: true}
});

bicicletaSchema.statics.allBicis = function (cb) {
    return this.find({}, cb);
}

bicicletaSchema.statics.add = function (aBici, cb) {
    return this.create(aBici, cb);
}

bicicletaSchema.statics.findById = function (biciId, cb) {
    return this.findOne({bicicletaID: biciId}, cb);
}

bicicletaSchema.statics.removeById = function (biciId, cb) {
    return this.deleteOne({bicicletaID: biciId}, cb);
}

module.exports = mongoose.model("Bicicleta", bicicletaSchema);