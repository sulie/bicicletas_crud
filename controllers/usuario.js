let Usuario = require("../models/Usuario");

module.exports = {
    // Encuentra todos los usuarios dentro de la BBDD y los muestra en la vista usuarios/index
    list: function(req,res,next){ 
        Usuario.find({}, function(err,usuarios) {
            if(err) res.send(500, err.message);
            res.render("usuarios/index", {usuarios: usuarios});
        });
    },

    // Encuentra el usuario a modificar y lo muestra en la vista usuarios/update, manteniendo persistencia de datos en el formulario de edición
    update_get: function(req,res, next) { 
        Usuario.findById(req.params.id, function (err, usuario) {
            res.render("usuarios/update", {errors:{}, usuario: usuario});
        });
    },

    // Toma los datos del formulario de edición, encuentra el usuario a modificar y realiza la actualización, luego si hay errores te reenvia al formulario, si no, a /usuarios
    update: function(req, res, next){
        let update_values = {nombre: req.body.nombre};
        Usuario.findByIdAndUpdate(req.params.id, update_values, function(err, usuario) {
            if (err) {
                console.log(err);
                res.render("usuario/update", {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            } else {
                res.redirect("/usuarios");
                return;
            }
        });
    },

    // Muestra la vista para la creación de usuarios
    create_get: function(req, res, next){
        res.render("usuarios/create", {errors:{}, usuario: new Usuario()});
    },

    // Crea el usuario a partir de los datos del formulario
    create: function(req, res, next) {
        // Comprueba que las contraseñas coincidan, si no coinciden, muestra la vista de nuevo y un mensaje de error, pero mantiene la persistencia en el formulario
        if (req.body.password != req.body.confirm_password) {
            res.render("usuarios/create", {errors: {confirm_password: {message: "No coincide con el password introducido."}}, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            return; // Para la ejecución de la función solo en caso de error
        }
        // Parte de la función que realiza la inserción 
        Usuario.create({nombre:req.body.nombre, email: req.body.email, password: req.body.password}, function(err, nuevoUsario) {
            // En caso de error, vuelve a la vista y mantiene la persistencia en el formulario
            if(err){
                res.render("usuarios/create", {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            } 
            // Si no hay ningún error, se envía un email de bienvenida y se redirige a /usuarios
            else {
                nuevoUsario.enviar_email_bienvenida();
                res.redirect("/usuarios");
            }
        });
    },

    // Encuentra el usuario a borrar y realiza el borrado, luego, si no hay errores, redirige /usuarios
    delete: function(req,res,next){
        Usuario.findByIdAndDelete(req.body.id, function(err){
            if(err) next(err);
            
            else res.redirect("/usuarios");
        });
    }
}