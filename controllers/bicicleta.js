let Bicicleta = require("../models/Bicicleta");

exports.bicicleta_list = function (req,res) {
    Bicicleta.allBicis(function (err, bicis) {
        if (err) res.send(500, err.message);
        res.render("bicicletas/index", {bicis});
    });
    
}

exports.bicicleta_create_get = function (req,res) {
    res.render("bicicletas/create");
}

exports.bicicleta_create_post = function (req,res) {
    let bici = new Bicicleta({
        bicicletaID: req.body.bicicletaID,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.latitud, req.body.longitud]
    });
    
    Bicicleta.add(bici, function (err, bici) {
        if (err) res.send(500, err.message);
        res.redirect("/bicicletas");
    });
}

exports.bicicleta_delete_post = function (req,res) {
    let bici = req.body.bicicletaID;

    Bicicleta.removeById(bici, function (err, bici) {
        if (err) res.send(500, err.message);
        res.redirect("/bicicletas");
    });
}

exports.bicicleta_update_get = function(req,res) {
    Bicicleta.findById(req.params.id, function (err, bici) { //Se usa params en vez de body porque viene por get (url)
        if (err) res.send(500, err.message);
        res.render("bicicletas/update", {bici});
    });
}

exports.bicicleta_update_post = function(req,res) {
    Bicicleta.findById(req.params.id, function (err, bici) {
        if (err) res.send(500, err.message);
        
        let modified = {
            bicicletaID: req.body.bicicletaID,
            color: req.body.color,
            modelo: req.body.modelo,
            ubicacion: [req.body.latitud, req.body.longitud]
        }
        Bicicleta.update({bicicletaID: bici.bicicletaID}, modified, function (error, doc) {
            if (error) res.send(500, err.message);
            res.redirect("/bicicletas");
        });
    });   
}