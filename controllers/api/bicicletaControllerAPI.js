let Bicicleta = require("../../models/Bicicleta");

exports.bicicleta_list = function(req,res) {
    Bicicleta.allBicis(function (err, bicis) {
        if (err) res.send(500, err.message);
        res.status(200).json({
            bicicletas: bicis
        });
    });
};

exports.bicicleta_create = function(req,res) {
    let bici = new Bicicleta({
        bicicletaID: req.body.bicicletaID,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.latitud, req.body.longitud]
    });

    Bicicleta.add(bici, function (err, bici) {
        if (err) res.send(500, err.message);
        res.status(201).json({ // Codigo 201 == CREATE con éxito
            bicicleta: bici
        })
    });
};

exports.bicicleta_delete = function(req,res){
    let bici = req.body.bicicletaID;

    Bicicleta.removeById(bici, function (err, bici) {
        if (err) res.send(500, err.message);
        res.status(204).send();
    });
}

exports.bicicleta_update = function(req,res){
    Bicicleta.findById(req.body.target, function (err, bici) {
        if (err) res.send(500, err.message);
        
        let modified = {
            bicicletaID: req.body.bicicletaID,
            color: req.body.color,
            modelo: req.body.modelo,
            ubicacion: [req.body.latitud, req.body.longitud]
        }
        Bicicleta.update({bicicletaID: bici.bicicletaID}, modified, function (error, doc) {
            if (error) res.send(500, err.message);
            res.status(200).send();
        });
    });
}