let Reserva = require("../../models/Reserva");

exports.reserva_list = function(req,res) {
    Reserva.allReservas(function (err, reservas) {
        if (err) res.send(500, err.message);
        res.status(200).json({
            reservas: reservas
        });
    });
};

exports.reserva_delete = function(req,res){
    let reserva = req.body._id;

    Reserva.removeById(reserva, function (err, reserva) {
        if (err) res.send(500, err.message);
        res.status(204).send();
    });
}

exports.reserva_update = function(req,res){
    Reserva.findById(req.body._id, function (err, reserva) {
        if (err) res.send(500, err.message);
        
        let modified = {
            usuario: req.body.usuario,
            bicicleta: req.body.bicicleta,
            desde: req.body.desde,
            hasta: req.body.hasta
        }

        Reserva.update({_id: reserva._id}, modified, function (error, doc) {
            if (error) res.send(500, err.message);
            res.status(200).send();
        });
    });
}