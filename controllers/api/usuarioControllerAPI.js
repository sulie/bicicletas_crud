let Usuario = require("../../models/Usuario");

exports.usuarios_list = function(req,res) {
    Usuario.allUsuarios(function (err, usuarios) {
        if (err) res.send(500, err.message);
        res.status(200).json({
            usuarios: usuarios
        });
    });
};

exports.usuarios_create = function(req,res) {
    let usuario = new Usuario({
        nombre: req.body.nombre,
    });

    Usuario.add(usuario, function (err, user) {
        if (err) res.send(500, err.message);
        res.status(201).json({ // Codigo 201 == CREATE con éxito
            usuario: user
        })
    });
};

exports.usuarios_delete = function(req,res){
    let usuario = req.body._id;

    Usuario.removeById(usuario, function (err, user) {
        if (err) res.send(500, err.message);
        res.status(204).send();
    });
}

exports.usuarios_update = function(req,res){
    Usuario.findById(req.body._id, function (err, user) {
        if (err) res.send(500, err.message);
        
        Usuario.update({_id: user._id}, { $set: {nombre: req.body.nombre }}, function (error, doc) {
            if (error) res.send(500, err.message);
            res.status(200).send();
        });
    });
}

exports.usuarios_reservar = function(req,res){ 

    Usuario.findById(req.body._id, function(err,usuario) {
        if(err) res.status(500).send(err.message);

        console.log(usuario);
        usuario.reservar(req.body.bicicletaID, req.body.desde, req.body.hasta, function(err){
        console.log("Reservada!!");
        res.status(200).send();
        });
    }); 
};