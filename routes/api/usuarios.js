let express = require('express');
let router = express.Router();
let usuarioControllerAPI = require("../../controllers/api/usuarioControllerAPI");

router.get("/", usuarioControllerAPI.usuarios_list);
router.post("/create", usuarioControllerAPI.usuarios_create);
router.post("/reservar", usuarioControllerAPI.usuarios_reservar);
router.delete("/delete", usuarioControllerAPI.usuarios_delete);
router.put("/update", usuarioControllerAPI.usuarios_update);
module.exports = router;