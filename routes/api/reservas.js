let express = require('express');
let router = express.Router();
let reservaControllerAPI = require("../../controllers/api/reservaControllerAPI");

router.get("/", reservaControllerAPI.reserva_list);
router.delete("/delete", reservaControllerAPI.reserva_delete);
router.put("/update", reservaControllerAPI.reserva_update);
module.exports = router;