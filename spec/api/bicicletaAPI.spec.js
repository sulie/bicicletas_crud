let server = require('../../bin/www');

let request = require('request');
let Bicicleta = require('../../models/Bicicleta');

describe ("Test de la API", () => {
    beforeEach(() => Bicicleta.allBicis = []);
    describe ("GET /", () =>{
        it("Devuelve status 200", () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            let a = new Bicicleta(1, "Rojo", "Orbea", [28.503789, -13.853296]);

            Bicicleta.add(a);

            request.get("http://localhost:3000/api/bicicletas", (error,response,body)=>{
                expect(response.statusCode).toBe(200);
            });
            expect(Bicicleta.allBicis.length).toBe(1);
            expect(Bicicleta.findById(1).color).toBe("Rojo");
        });
    });
    describe ("POST /create", () =>{
        it("Devuelve status 201", (done) => {

            let header = {"Content-type": "application/JSON"};
            let aBici ='{"id": 2, "color": "Rojo", "modelo": "Trek", "latitud": 29, "longitud": -14}';

            request.post({

                headers: header,
                url: "http://localhost:3000/api/bicicletas/create",
                body: aBici

            }, (error,response,body)=>{
                expect(response.statusCode).toBe(201);
                expect(Bicicleta.findById(2).color).toBe("Rojo");
                done();
            }); 
        });
    });
    describe ("DELETE /delete", () =>{
        it("Devuelve status 204", (done) => {
            let a = new Bicicleta(2, "Rojo", "Trek", [22.503789, -12.853296]);
            Bicicleta.add(a);

            let headers = {"Content-type": "application/JSON"};
            let aBici ='{"id": 2}';            

            request.delete({
                headers: headers,
                url: "http://localhost:3000/api/bicicletas/delete",
                body: aBici
            }, (error,response,body)=>{
                expect(response.statusCode).toBe(204);
                expect(Bicicleta.allBicis.length).toBe(0);
                done();
            });
        });
    });

    describe ("PUT /update", () =>{
        it("Devuelve status 200", (done) => {
            let a = new Bicicleta(3, "Rojo", "Trek", [22.503789, -12.853296]);
            Bicicleta.add(a);

            let headers = {"Content-type": "application/JSON"};
            let aBici ='{"id": 3, "color": "Azul", "modelo": "Mountain", "ubicacion": [29.503789, -14.853296]}';

            request.put({
                headers: headers,
                url: "http://localhost:3000/api/bicicletas/update",
                body: aBici
            }, (error,response,body)=>{
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(3).modelo).toBe("Mountain");
                done();
            });
        });
    });
});