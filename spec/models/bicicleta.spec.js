let mongoose = require("mongoose");
let Bicicleta = require("../../models/Bicicleta");

describe("Testing unitario Bicicletas", function(){
    beforeEach(function (done) {

        var mongoDB = "mongodb://localhost/red_bicicletas";
        mongoose.connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true  }); 

        var db = mongoose.connection;

        db.on("error", console.error.bind('Error de conexión con MongoDB'));
        db.once("open", function () {
            console.log("Conectado a la BBDD");
            done();
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe("Bicicleta.allBicis", () => {
        it("Empieza vacío", (done)=> {
            Bicicleta.allBicis(function (err, bicis) {            
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe("Bicicleta.add", () => {
        it("agrega sólo una bici", (done)=> {
            let aBici = new Bicicleta({bicicletaID: 1, color: "verde", modelo: "urbana"});
            Bicicleta.add (aBici, (err,newBici)=>{
                if (err) console.log(err);
                Bicicleta.allBicis((err,bicis)=>{

                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].bicicletaID).toEqual(aBici.bicicletaID);
                    done();
                });
            });                
        });        
    });

    describe('Bicicleta.findById', () => {
        it('Encuentra la bici con bicicletaID 1', (done) => {
            let bici = new Bicicleta({
                bicicletaID: 1,
                color: "Rojo",
                modelo: "Orbea",
                ubicacion: [33, -10]
            });
            Bicicleta.add(bici, function (err, bici) {
                if (err) console.log(err);

                Bicicleta.findById(1, function (err, bici) {
                    if (err) console.log(err);
    
                    expect(bici.bicicletaID).toBe(1);
                    done();
                });
            });            
        });
    });

    describe('Bicicleta.removeById', () => {
        it('Elimina la bici con bicicletaID 1', (done) => {
            let bici = new Bicicleta({
                bicicletaID: 1,
                color: "Rojo",
                modelo: "Orbea",
                ubicacion: [33, -10]
            });
            Bicicleta.add(bici, function (err, bici) {
                if (err) console.log(err);
                
                Bicicleta.removeById(1, function (err, bici) {
                    if (err) console.log(err);
    
                    Bicicleta.allBicis(function (err, bicis) {            
                        expect(bicis.length).toBe(0);
                        done();
                    });
                });
            });            
        });
    });
});  